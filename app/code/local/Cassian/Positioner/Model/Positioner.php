<?php

/**
 * Class Cassian_Positioner_Model_Positioner
 * @author Hector Fernandez <hector.nek@gmail.com>
 * @access Public
 * @package Cassian_Positioner
 * @copyright Copyright (c) 2018 X.commerce, Inc. and affiliates (http://www.magento.com)
 */
class Cassian_Positioner_Model_Positioner extends Mage_Core_Model_Abstract
{
    /**
     * @var array $csvHeaders
     */
    private $csvHeaders = [
        'category_id' , 'sku', 'position'
    ];

    /**
     * Cassian_Positioner_Model_Positioner Constructor
     * Init Model interface
     */
    protected function _construct(){

       $this->_init("positioner/positioner");

    }

    /**
     * Process file to save positions
     * @param $fileName
     * @return bool
     */
    public function processFile($fileName){
        /**
         * Initialize bool response as FALSE
         */
        $boolResponse = False;
        try {
            /** Check if file exists */
            if(file_exists($fileName)){
                /** @var Varien_File_Csv $csv */
                $csv = new Varien_File_Csv();
                /** @var array $data  */
                $data = $csv->getData($fileName);
                /** @var array $columns - Get columns names */
                $columns = array_shift($data);
                /** check if file columns are allowed */
                if($this->csvHeaders === $columns){
                    /** @var  $product Loads catalog product entity model */
                    $product = Mage::getModel("catalog/product");
                    /** @var  $categoryApi Load Api interface*/
                    $categoryApi = Mage::getSingleton('catalog/category_api');
                    /** iteration over the data fields*/
                    for($i=0, $t = count($data); $i < $t; $i++ ){
                        /** @var  int $categoryId */
                        $categoryId = (int)$data[$i][0];
                        /** @var STRING $sku */
                        $sku = (string)$data[$i][1];
                        /** @var INT $position */
                        $position = (int)$data[$i][2];
                        /** @var INT $productEntityId  Loaded from product model*/
                        $productEntityId = $product->getIdBySku($sku);
                        /** Assign product to category with position */
                        $categoryApi->assignProduct($categoryId,$productEntityId,$position);

                    }
                    /** @var BOOL $boolResponse Change to true */
                    $boolResponse = TRUE;
                }else{
                    Mage::log("{$fileName} does not have the right columns names ",NULL,'cassian_positioner.log');
                }

            }else{
                Mage::log("{$fileName} does not exist ",NULL,'cassian_positioner.log');
            }

        } catch (Exception $exception) {
            Mage::log($exception->getMessage() . PHP_EOL . $exception->getTraceAsString(),NULL,'cassian_positioner.log');
        }
        return $boolResponse;
    }


}
	 