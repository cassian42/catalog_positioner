<?php

/**
 * Class Cassian_Positioner_Model_Mysql4_Positioner
 * @author Hector Fernandez <hector.nek@gmail.com>
 * @access Public
 * @package Cassian_Positioner
 * @copyright Copyright (c) 2018 X.commerce, Inc. and affiliates (http://www.magento.com)
 */
class Cassian_Positioner_Model_Mysql4_Positioner extends Mage_Core_Model_Mysql4_Abstract
{
    /**
     * Cassian_Positioner_Model_Mysql4_Positioner constructor
     * set primary key for collections
     */
    protected function _construct()
    {
        $this->_init("positioner/positioner", "id");
    }
}