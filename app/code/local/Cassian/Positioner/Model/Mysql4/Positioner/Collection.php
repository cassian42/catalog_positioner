<?php

/**
 * Class Cassian_Positioner_Model_Mysql4_Positioner_Collection
 * @author Hector Fernandez <hector.nek@gmail.com>
 * @access Public
 * @package Cassian_Positioner
 * @copyright Copyright (c) 2018 X.commerce, Inc. and affiliates (http://www.magento.com)
 */
    class Cassian_Positioner_Model_Mysql4_Positioner_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
    {
        /**
         * Cassian_Positioner_Model_Mysql4_Positioner_Collection Constructor
         * initialize model
         */
		public function _construct(){
			$this->_init("positioner/positioner");
		}

		

    }
	 