<?php
/**
 * Instaler
 * @author Hector Fernandez <hector.nek@gmail.com>
 * @access Public
 * @package Cassian_Positioner
 * @copyright Copyright (c) 2018 X.commerce, Inc. and affiliates (http://www.magento.com)
 */
/**
 * Init installer
 */
$installer = $this;
/**
 * start Setup
 */
$installer->startSetup();
/**
 * Installation Query
 * @table: cassian_catalog_massive_positioner
 */
$sql=<<<SQLTEXT
CREATE TABLE `cassian_catalog_massive_positioner` (
`id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
`filename` varchar(255) NOT NULL,
`user` varchar(255) NOT NULL,
`creation_date` TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
`process_state` VARCHAR(25) DEFAULT 'PENDING',
`processed_date` TIMESTAMP
)ENGINE=INNODB;
		
SQLTEXT;
/**
 * execute installer
 */
$installer->run($sql);
/**
 * close Connection.
 */
$installer->endSetup();
	 