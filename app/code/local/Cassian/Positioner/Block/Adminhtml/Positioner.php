<?php
/**
 * Class Cassian_Positioner_Block_Adminhtml_Positioner
 * @author Hector Fernandez <hector.nek@gmail.com>
 * @access Public
 * @package Cassian_Positioner
 * @copyright Copyright (c) 2018 X.commerce, Inc. and affiliates (http://www.magento.com)
 */

class Cassian_Positioner_Block_Adminhtml_Positioner extends Mage_Adminhtml_Block_Widget_Grid_Container{
    /**
     * Cassian_Positioner_Block_Adminhtml_Positioner constructor.
     * INIT
     */
	public function __construct()
	{
        /**
         * Set header text and button text.
         */
	$this->_controller = "adminhtml_positioner";
	$this->_blockGroup = "positioner";
	$this->_headerText = Mage::helper("positioner")->__("Files Manager");
	$this->_addButtonLabel = Mage::helper("positioner")->__("Add New File");
	parent::__construct();
	
	}

}