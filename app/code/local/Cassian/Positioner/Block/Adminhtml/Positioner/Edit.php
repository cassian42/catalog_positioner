<?php

/**
 * Class Cassian_Positioner_Block_Adminhtml_Positioner_Edit
 * @author Hector Fernandez <hector.nek@gmail.com>
 * @access Public
 * @package Cassian_Positioner
 * @copyright Copyright (c) 2018 X.commerce, Inc. and affiliates (http://www.magento.com)
 */
class Cassian_Positioner_Block_Adminhtml_Positioner_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    /**
     * Cassian_Positioner_Block_Adminhtml_Positioner_Edit constructor.
     */
		public function __construct()
		{

				parent::__construct();
				$this->_objectId = "id";
				$this->_blockGroup = "positioner";
				$this->_controller = "adminhtml_positioner";
				$this->_updateButton("save", "label", Mage::helper("positioner")->__("Save File"));
				$this->_updateButton("delete", "label", Mage::helper("positioner")->__("Delete File"));

				$this->_addButton("saveandcontinue", array(
					"label"     => Mage::helper("positioner")->__("Save And Continue Edit"),
					"onclick"   => "saveAndContinueEdit()",
					"class"     => "save",
				), -100);



				$this->_formScripts[] = "

							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
						";
		}

    /**
     * @return string
     */
		public function getHeaderText()
		{
				if( Mage::registry("positioner_data") && Mage::registry("positioner_data")->getId() ){

				    return Mage::helper("positioner")->__("Edit file '%s'", $this->htmlEscape(Mage::registry("positioner_data")->getFilename()));

				} 
				else{

				     return Mage::helper("positioner")->__("Add File");

				}
		}
}