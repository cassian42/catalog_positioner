<?php

/**
 * Class Cassian_Positioner_Adminhtml_PositionerController
 * @author Hector Fernandez <hector.nek@gmail.com>
 * @access Public
 * @package Cassian_Positioner
 * @copyright Copyright (c) 2018 X.commerce, Inc. and affiliates (http://www.magento.com)
 */
class Cassian_Positioner_Adminhtml_PositionerController extends Mage_Adminhtml_Controller_Action {
    /**
     * @return bool
     */
    protected function _isAllowed() {
        //return Mage::getSingleton('admin/session')->isAllowed('positioner/positioner');
        return true;
    }

    /**
     * @return $this
     */
    protected function _initAction() {
        $this->loadLayout()->_setActiveMenu("positioner/positioner")->_addBreadcrumb(Mage::helper("adminhtml")->__("Positioner  Manager"), Mage::helper("adminhtml")->__("Positioner Manager"));
        return $this;
    }

    /**
     * Render index layout
     */
    public function indexAction() {
        $this->_title($this->__("Catalog Category Positioner"));
        $this->_title($this->__("Manager"));

        $this->_initAction();
        $this->renderLayout();
    }

    /**
     * @throws Mage_Core_Exception
     */
    public function editAction() {
        $this->_title($this->__("Positioner"));
        $this->_title($this->__("Positioner"));
        $this->_title($this->__("Edit File"));

        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("positioner/positioner")->load($id);
        if ($model->getId()) {
            Mage::register("positioner_data", $model);
            $this->loadLayout();
            $this->_setActiveMenu("positioner/positioner");
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Positioner Manager"), Mage::helper("adminhtml")->__("Manager"));
            $this->_addBreadcrumb(Mage::helper("adminhtml")->__("Positioner Description"), Mage::helper("adminhtml")->__("Description"));
            $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()->createBlock("positioner/adminhtml_positioner_edit"))->_addLeft($this->getLayout()->createBlock("positioner/adminhtml_positioner_edit_tabs"));
            $this->renderLayout();
        } else {
            Mage::getSingleton("adminhtml/session")->addError(Mage::helper("positioner")->__("File does not exist."));
            $this->_redirect("*/*/");
        }
    }

    /**
     * @throws Mage_Core_Exception
     */
    public function newAction() {

        $this->_title($this->__("Positioner"));
        $this->_title($this->__("Positioner"));
        $this->_title($this->__("New File"));

        $id = $this->getRequest()->getParam("id");
        $model = Mage::getModel("positioner/positioner")->load($id);

        $data = Mage::getSingleton("adminhtml/session")->getFormData(true);
        if (!empty($data)) {
            $model->setData($data);
        }

        Mage::register("positioner_data", $model);

        $this->loadLayout();
        $this->_setActiveMenu("positioner/positioner");

        $this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

        $this->_addBreadcrumb(Mage::helper("adminhtml")->__(" Manager"), Mage::helper("adminhtml")->__(" Manager"));
        $this->_addBreadcrumb(Mage::helper("adminhtml")->__(" Description"), Mage::helper("adminhtml")->__(" Description"));


        $this->_addContent($this->getLayout()->createBlock("positioner/adminhtml_positioner_edit"))->_addLeft($this->getLayout()->createBlock("positioner/adminhtml_positioner_edit_tabs"));

        $this->renderLayout();
    }

    /**
     * function saveAction
     * save data on database
     */
    public function saveAction() {
        /**
         * get post data
         */
        $post_data = $this->getRequest()->getPost();
        /**
         * validate post data existence.
         */

        if ($post_data) {

            try {
                /** Start save file  * */
                try {
                    /**
                     * validate delete instruction.
                     */
                    if ((bool) $post_data['filename']['delete'] == 1) {
                        /**
                         * unset filename
                         */
                        $post_data['filename'] = '';

                    } else {
                        /**
                         * unset filename
                         */
                        unset($post_data['filename']);
                        /**
                         * validate $_FILES Existence
                         */
                        if (!empty($_FILES)) {
                            /**
                             * Validate System file name
                             */
                            if ($_FILES['filename']['name']) {
                                /**
                                 * for update purposes.
                                 * verify previous id on URL
                                 */
                                if ($this->getRequest()->getParam("id")) {
                                    /**
                                     * Load file data
                                     */
                                    $model = Mage::getModel("positioner/positioner")->load($this->getRequest()->getParam("id"));
                                    /**
                                     * check file loaded
                                     */
                                    if ($model->getData('filename')) {
                                        /**
                                         * Varien_Io_File
                                         */
                                        $io = new Varien_Io_File();
                                        /**
                                         * remove previous file.
                                         */
                                        $io->rm(Mage::getBaseDir('media') . DS . implode(DS, explode('/', $model->getData('filename'))));
                                    }
                                }
                                /**
                                 * media file path
                                 */
                                $path = Mage::getBaseDir('media') . DS . 'positioner' . DS . 'positioner' . DS;
                                /**
                                 * Varien_File_Uploader
                                 * fileId: Filename
                                 */
                                $uploader = new Varien_File_Uploader('filename');
                                /**
                                 * Validate allowed Extension
                                 */
                                $uploader->setAllowedExtensions(array('csv'));
                                /**
                                 * disable renaming
                                 */
                                $uploader->setAllowRenameFiles(false);
                                /**
                                 * eliminate dispersion
                                 */
                                $uploader->setFilesDispersion(false);
                                /**
                                 * Set destination file name
                                 */
                                $destinationFile = $path . $_FILES['filename']['name'];
                                /**
                                 * set name to uploader
                                 */
                                $filename = $uploader->getNewFileName($destinationFile);
                                /**
                                 * Save file
                                 */
                                $uploader->save($path, $filename);
                                /**
                                 * set filename to save in database
                                 */
                                $post_data['filename'] = 'positioner/positioner/' . $filename;
                            }
                        }
                    }
                } catch (Exception $e) {
                    /**
                     * Redirect and set error message
                     */
                    Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                    $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                    return;
                }
                /**    End file save    * */
                /**
                 * User information.
                 */
                $post_data['user'] = null;
                /**
                 * Get current user
                 */
                /** @var admin/session $admin */
                $admin = Mage::getSingleton('admin/session')->getUser();
                /**
                 * if session user info was correctly loaded
                 */
                if(!empty($admin->getId()))
                {
                    /** Set Username to post_data */
                    $post_data['user'] = $admin->getUsername();
                }

                /**
                 * Load posiotioner Model
                 */
                $model = Mage::getModel("positioner/positioner");

                /**
                 * Check if positioner is enable
                 */
                $isEnabled = Mage::getStoreConfig('catalog_positioner/configuration/status');
                if((bool)$isEnabled and !empty($path)){
                        /** Process File */
                        $wasProcessed = $model->processFile(Mage::getBaseDir('media') . DS . $post_data['filename']);
                        if($wasProcessed){
                            $post_data['process_state'] = 'SUCCESS';
                            $post_data['processed_date'] = date('Y-m-d H:i:s');
                        }else{
                            Mage::getSingleton('adminhtml/session')->addError(Mage::helper("adminhtml")->__('CSV Format Error please recheck your file and try again'));
                            $post_data['process_state'] = 'ERROR';
                        }
                }else{
                    Mage::getSingleton('adminhtml/session')->addError(Mage::helper("adminhtml")->__('Positioner is not enabled, please go to System -> Configuration -> Catalog -> Positioner Or You don\'t have enough permissions over media directory'));
                }
                /** @var  $model positioner/positioner
                 * Save in database
                 */
                $model->addData($post_data)
                        ->setId($this->getRequest()->getParam("id"))
                        ->save();
                /**
                 * set header message
                 */
                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("File information was successfully saved"));
                /**
                 * set session data
                 */
                Mage::getSingleton("adminhtml/session")->setPositionerData(false);
                /**
                 * redirect process
                 */
                if ($this->getRequest()->getParam("back")) {
                    $this->_redirect("*/*/edit", array("id" => $model->getId()));
                    return;
                }
                $this->_redirect("*/*/");
                return;
            } catch (Exception $e) {
                /**
                 * error handler
                 */
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                Mage::getSingleton("adminhtml/session")->setPositionerData($this->getRequest()->getPost());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
                return;
            }
        }
        $this->_redirect("*/*/");
    }

    public function deleteAction() {
        if ($this->getRequest()->getParam("id") > 0) {
            try {
                $model = Mage::getModel("positioner/positioner");
                $model->setId($this->getRequest()->getParam("id"))->delete();
                Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
                $this->_redirect("*/*/");
            } catch (Exception $e) {
                Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
                $this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
            }
        }
        $this->_redirect("*/*/");
    }

    public function massRemoveAction() {
        try {
            $ids = $this->getRequest()->getPost('ids', array());
            foreach ($ids as $id) {
                $model = Mage::getModel("positioner/positioner");
                $model->setId($id)->delete();
            }
            Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item(s) was successfully removed"));
        } catch (Exception $e) {
            Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
        }
        $this->_redirect('*/*/');
    }

    /**
     * Export order grid to CSV format
     */
    public function exportCsvAction() {
        $fileName = 'positioner.csv';
        $grid = $this->getLayout()->createBlock('positioner/adminhtml_positioner_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }

    /**
     *  Export order grid to Excel XML format
     */
    public function exportExcelAction() {
        $fileName = 'positioner.xml';
        $grid = $this->getLayout()->createBlock('positioner/adminhtml_positioner_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getExcelFile($fileName));
    }

}
